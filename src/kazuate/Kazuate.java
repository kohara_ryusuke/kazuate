package kazuate;

import java.util.Scanner;

public class Kazuate {
	public static void main(String[] args) {

		int[] ans = answer();
		int count = 0;
		int userInputAns = 0;
		System.out.println("数あてゲームです。");
		System.out.println("3桁の被らない数字(1～9)を入力してください。");
		System.out.println("10回以内に当ててね！");
		boolean clearFlg = false;

		while(count < 10) {
			System.out.println(count + 1 + ">");
			userInputAns = getNumberInput();
			System.out.println("..... ");
			System.out.println("あなたの入力した値は" + userInputAns + "です。");

			String[] answerCheck = answerCheck(ans, userInputAns);
			int hit = 0;
			int bite = 0;

			for(int i = 0; i < answerCheck.length; i++) {
				if(answerCheck[i] == "1") {
					hit++;
				}else if(answerCheck[i] == "2") {
					bite++;
				}
			}

			System.out.println(userInputAns + "は、ヒットが" + hit + "つ、バイトが" + bite + "です。");

			if(hit == ans.length) {
				clearFlg = true;;
				break;
			}else {
				System.out.println("残念、、がんばって！！");
			}
			count++;
		}

		if(clearFlg) {
			System.out.println("正解！！すごい！！！");
		}else {
			System.out.println("Game Over! 正解は " + ans[0] + ans[1] + ans[2] + " でした。");
		}

	}

	private static String[] answerCheck(int[] ans, int userInputAns) {
		int[] userAns = new int[3];
		String[] answerCheck = new String[3];

		userAns[2] = (userInputAns % 10);
		userInputAns /= 10;
		userAns[1] = (userInputAns % 10);
		userInputAns /= 10;
		userAns[0] = (userInputAns % 10);

		for(int i = 0; i < ans.length ;i++) {
			if(ans[i] == userAns[i]) {
				answerCheck[i] = "1";
			}else {
				for(int j = 0; j < ans.length; j++) {
					if (ans[j] == userAns[i]) {
						answerCheck[i] = "2";
					}
				}
			}
			if(answerCheck[i] == null) {
				answerCheck[i] = "0";
			}
		}
		return answerCheck;
	}

	private static int getNumberInput() {
		int answer = 0;
		Scanner scan = new Scanner(System.in);
		while(true) {
			String obj = scan.next();
			if(obj.matches("[0-9]{3}")) {
				answer = Integer.parseInt(obj);
				break;
			}else {
				System.out.println("3桁の数字(0～9)を入力してください。");
			}
		}
		return answer;
	}

	private static int[] answer() {
		int[] answer = new int[3];
		for(int i = 0; i < answer.length; i++){
			Loop: while(true){
				answer[i] = (int)(Math.random() * 10) + 1;  //0~9の数値を入れる
				for(int j = 0; j < i; j++){
					if(answer[j] == answer[i]) continue Loop;
				}
				break;
			}
		}
		return answer;
	}


}
